package org.example;

import org.example.image.ImageFileProcessor;
import org.example.image.ImageFileProcessor2;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Main {

    public static void main(String[] args) {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        ImageFileProcessor imageFileProcessor = container.select(ImageFileProcessor.class).get();
        ImageFileProcessor2 imageFileProcessor2 = container.select(ImageFileProcessor2.class).get();

        System.out.println(imageFileProcessor.openFile("file1.png"));
        System.out.println(imageFileProcessor2.openFile("file1.jpg"));

        container.shutdown();
    }

}
