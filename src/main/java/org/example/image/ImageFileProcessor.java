package org.example.image;

import org.example.image.qualifier.JpgFileEditorQualifier;

import javax.inject.Inject;

public class ImageFileProcessor {

    private ImageFileEditor imageFileEditor;
    private TimeLogger timeLogger;

    @Inject
    public ImageFileProcessor(ImageFileEditor imageFileEditor, TimeLogger timeLogger) {
        this.imageFileEditor = imageFileEditor;
        this.timeLogger = timeLogger;
    }

    public String openFile(String fileName) {
        return imageFileEditor.openFile(fileName) + " at: " + timeLogger.getTime();
    }
}
