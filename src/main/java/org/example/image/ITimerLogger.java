package org.example.image;

public interface ITimerLogger {
    String getTime();
}
