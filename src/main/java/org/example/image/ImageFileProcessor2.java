package org.example.image;

import org.example.image.qualifier.JpgFileEditorQualifier;

import javax.inject.Inject;

public class ImageFileProcessor2 {
    private ImageFileEditor imageFileEditor;
    private TimeLogger timeLogger;

    @Inject
    public ImageFileProcessor2(@JpgFileEditorQualifier ImageFileEditor imageFileEditor, TimeLogger timeLogger) {
        this.imageFileEditor = imageFileEditor;
        this.timeLogger = timeLogger;
    }

    public String openFile(String fileName) {
        return imageFileEditor.openFile(fileName) + " at: " + timeLogger.getTime();
    }
}
